#ifndef LEXER_H_
#define LEXER_H_

#include <stdlib.h>

typedef enum TokenKind {
  tk_heading,    // # 0
  tk_link,       // @ 1
  tk_image,      // ! 2
  tk_code,       // ` 3
  tk_bold,       // * 4
  tk_italics,    // / 5
  tk_underline,  // _ 6
  tk_strike,     // ~ 7
  tk_line,       // - 8
  tk_url_start,  // ( 9
  tk_url_end,    // ) 10
  tk_name_start, // [ 11
  tk_name_end,   // ] 12
  tk_newline,    // \n 13
  tk_break,      // ; 14
  tk_list,       // + 15
  tk_escape,
} TokenKind;

typedef struct Token {
  TokenKind kind;
  int position;
  int line;
  struct Token *next;
} Token;

Token *get_tokens(char *source);

#endif // LEXER_H_
