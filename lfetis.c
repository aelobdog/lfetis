#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "lexer.h"
#include "parser.h"

int main(int argc, char **argv) {
  char *input;
  int inputSize, errors;
  FILE *out;

  int bold_open = 0;
  int italics_open = 0;
  int strike_open = 0;
  int underline_open = 0;
  int heading_open = 0;
  int list_open = 0;

  if (argc < 4) {
    perror("insufficient arguments");
  }
  int infd = open(argv[1], O_RDONLY, S_IRUSR | S_IWUSR);
  struct stat sb;

  if (fstat(infd, &sb) == -1) {
    perror("getting file size");
  }

  printf("proc: %s\n", argv[1]);
  out = fopen(argv[2], "w");
  inputSize = sb.st_size;
  input = (char *)mmap(NULL, inputSize, PROT_READ, MAP_PRIVATE, infd, 0);

  Token *tokens = get_tokens(input);
  // Token *hi = tokens;
  // while (hi != NULL) {
  //   printf("%d > %d\n", hi->position, hi->kind);
  //   hi = hi->next;
  // }

  parse(tokens);
  printf("\t%sOk%s\n", "\e[1;32m", "\e[0m");

  fputs("<!doctype HTML>\n<html>\n<head>\n<title> Aelobdog's Personal Website </title>\n <meta charset=\"UTF-8\">\n <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n<link rel=\"stylesheet\" href=\"", out);
  fputs(argv[3], out);
  fputs("\">\n</head>\n<body>\n<div class=\"content\">\n", out);
  Token *iter = tokens;
  int start = 0;
  int lv;
  fwrite(input, iter->position - start, 1, out);
  start = iter->position;

  char *display, *url;
  int display_len, url_len;

  while (iter->next != NULL) {
    if (start != iter->position) {
      fwrite(input + start, iter->position - start, 1, out);
      start = iter->position;
    }

    switch (iter->kind) {
    case tk_escape:
      // fputc(input[iter->position + 1], out);
      if (iter->next->position == iter->position + 1)
        iter = iter->next;
      start = iter->position + 1;
      break;

    case tk_heading:
      lv = 1;
      while (iter->next->kind == tk_heading) {
        lv++;
        iter = iter->next;
      }
      char heading[6];
      sprintf(heading, "<h%d>", lv);
      fputs(heading, out);
      heading_open = 1;
      start = iter->position + 1;
      break;

    case tk_break:
      fputs("<br>\n", out);
      start = iter->position + 1;
      break;

    case tk_list:
      list_open = 1;
      fputs("<li>", out);
      start = iter->position + 1;
      break;

    case tk_link:
      fputs("<a href=\"", out);
      iter = iter->next; // at the [
      display = input + iter->position + 1;
      display_len = iter->next->position - iter->position - 1;
      iter = iter->next; // at the ]
      iter = iter->next; // at the (
      url = input + iter->position + 1;
      url_len = iter->next->position - iter->position - 1;
      iter = iter->next; // at the )
      fwrite(url, url_len, 1, out);
      fputs("\">", out);
      fwrite(display, display_len, 1, out);
      fputs("</a>", out);
      start = iter->position + 1;
      break;

    case tk_image:
      fputs("<img src=\"", out);
      iter = iter->next; // at the [
      display = input + iter->position + 1;
      display_len = iter->next->position - iter->position;
      iter = iter->next; // at the ]
      iter = iter->next; // at the (
      url = input + iter->position + 1;
      url_len = iter->next->position - iter->position;
      iter = iter->next; // at the )
      fwrite(url, url_len, 1, out);
      fputs("\" alt=\"", out);
      fwrite(display, display_len, 1, out);
      fputs("\">", out);
      start = iter->position + 1;
      break;

    case tk_code:
      fputs("<pre>", out);
      start = iter->position + 1;
      iter = iter->next;
      while (iter->next != NULL && iter->kind != tk_code) {
        iter = iter->next;
      }
      fwrite(input + start, iter->position - start, 1, out);
      fputs("</pre>", out);
      start = iter->position + 1;
      break;

    case tk_bold:
      if (bold_open) {
        fputs("</b>", out);
        bold_open = 0;
        start = iter->position + 1;
      } else {
        fputs("<b>", out);
        bold_open = 1;
        start = iter->position + 1;
      }
      break;

    case tk_italics:
      if (italics_open) {
        fputs("</i>", out);
        italics_open = 0;
        start = iter->position + 1;
      } else {
        fputs("<i>", out);
        italics_open = 1;
        start = iter->position + 1;
      }
      break;

    case tk_underline:
      if (underline_open) {
        fputs("</u>", out);
        underline_open = 0;
        start = iter->position + 1;
      } else {
        fputs("<u>", out);
        underline_open = 1;
        start = iter->position + 1;
      }
      break;

    case tk_strike:
      if (strike_open) {
        fputs("</s>", out);
        strike_open = 0;
        start = iter->position + 1;
      } else {
        fputs("<s>", out);
        strike_open = 1;
        start = iter->position + 1;
      }
      break;

    case tk_line:
      if (iter->next != NULL && iter->next->kind == tk_line) {
        iter = iter->next;
        if (iter->next != NULL && iter->next->kind == tk_line) {
          fputs("<hr>", out);
          iter = iter->next;
          start = iter->position + 1;
        }
      }
      break;

      // These dont need to be processed, but the compiler complains
      // if they're not included.
    case tk_url_start:
    case tk_url_end:
    case tk_name_start:
    case tk_name_end:
      break;

    case tk_newline:
      if (heading_open) {
        char heading[6];
        sprintf(heading, "</h%d>", lv);
        fputs(heading, out);
        heading_open = 0;
      } else if (list_open) {
        fputs("</li>", out);
        list_open = 0;
      }
      start = iter->position + 1;
      break;
    }

    iter = iter->next;
  }

  fputs("</div>\n</body>\n</html>\n", out);
  iter = tokens;
  while (iter != NULL) {
    // printf("%d > %d\n", iter->position, iter->kind);
    Token *del = iter;
    iter = iter->next;
    free(del);
  }
  fclose(out);
  munmap(input, inputSize);
  return 0;
}
