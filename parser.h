#ifndef PARSER_H_
#define PARSER_H_

#include "lexer.h"

int parse(Token* tokens);

#endif // PARSER_H_
