#include "parser.h"
#include "lexer.h"
#include <stdio.h>

int parse(Token *tokens) {
  int errors = 0;

  /*
  ** parsing is implemented to only check the validity of syntax for
  ** the link and image tags.
  */
  while (tokens->next != NULL) {
    switch (tokens->kind) {
    case tk_link:
    case tk_image:
      if (!((tokens->next->position == tokens->position + 1) &&
            (tokens->next->kind == tk_name_start))) {
        errors++;
        printf("error: line %d: syntax error in link/image - missing '['\n",
               tokens->line);
      }
      tokens = tokens->next; // at this point, tokens points to a [ after an @

      int open = 1;
      while (tokens != NULL && tokens->next != NULL) {
        if (tokens->next->kind == tk_name_start) {
          open++;
        } else if (tokens->next->kind == tk_name_end) {
          open--;
          if (!open) {
            break;
          }
        }
        Token* temp = tokens->next;
        tokens->next = tokens->next->next;
        free(temp);
      }
      // at this point, tokens points to token before the ]
      tokens = tokens->next; // tokens points to the ]

      if (!((tokens->next->position == tokens->position + 1) &&
            (tokens->next->kind == tk_url_start))) {
        errors++;
        printf("error: line %d: syntax error in link/image - missing '('\n",
               tokens->line);
      }
      tokens = tokens->next; // at this point, tokens points to a ( after a ]

      open = 1;
      while (tokens != NULL && tokens->next != NULL) {
        if (tokens->next->kind == tk_url_start) {
          open++;
        } else if (tokens->next->kind == tk_url_end) {
          open--;
          if (!open) {
            break;
          }
        }
        Token* temp = tokens->next;
        tokens->next = tokens->next->next;
        free(temp);
      }
      // at this point, tokens points to token before the )
      tokens = tokens->next; // tokens points to the )
      break;

    default:
      break;
    }
    tokens = tokens->next;
  }
  return errors;
}
