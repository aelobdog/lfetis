#include "lexer.h"

#include <stdio.h>

/*
** get_tokens works by scanning the input string for modifier characters like
** the asterisk or open paranthesis and records their locations in a linked
*list.
** The rest of the program is assumed to be plain text anyways and parsing can
** be done just by monitoring the relative positions of the tokens returned.
**
** when scanning, the lexer does not exclude urls from getting scanned, which
** leads to all the '/'s in the url to get recognized as tokens. This will be
** taken care of while parsing.
*/
Token *get_tokens(char *source) {
  Token *tokens = (Token *)malloc(sizeof(Token));
  Token *iter = tokens;
  int line = 1;

  for (int i = 0; source[i] != '\0'; i++) {
    switch (source[i]) {
    case '\\':
      iter->position = i;
      iter->kind = tk_escape;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      i++;
      break;

    case '*':
      iter->position = i;
      iter->kind = tk_bold;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '/':
      iter->position = i;
      iter->kind = tk_italics;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '_':
      iter->position = i;
      iter->kind = tk_underline;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '~':
      iter->position = i;
      iter->kind = tk_strike;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '[':
        iter->line = line;
        iter->position = i;
        iter->kind = tk_name_start;
        iter->next = (Token *)malloc(sizeof(Token));
        iter = iter->next;
      break;

    case ']':
        iter->line = line;
        iter->position = i;
        iter->kind = tk_name_end;
        iter->next = (Token *)malloc(sizeof(Token));
        iter = iter->next;
      break;

    case '(':
        iter->line = line;
        iter->position = i;
        iter->kind = tk_url_start;
        iter->next = (Token *)malloc(sizeof(Token));
        iter = iter->next;
      break;

    case ')':
        iter->line = line;
        iter->position = i;
        iter->kind = tk_url_end;
        iter->next = (Token *)malloc(sizeof(Token));
        iter = iter->next;
      break;

    case '!':
      iter->line = line;
      iter->position = i;
      iter->kind = tk_image;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '@':
      iter->line = line;
      iter->position = i;
      iter->kind = tk_link;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '#':
      iter->position = i;
      iter->kind = tk_heading;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '`':
      iter->position = i;
      iter->kind = tk_code;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '-':
      iter->position = i;
      iter->kind = tk_line;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case ';':
      iter->position = i;
      iter->kind = tk_break;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '+':
      iter->position = i;
      iter->kind = tk_list;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;

    case '\n':
      line++;
      iter->position = i;
      iter->kind = tk_newline;
      iter->next = (Token *)malloc(sizeof(Token));
      iter = iter->next;
      break;
    }
  }

  return tokens;
}
